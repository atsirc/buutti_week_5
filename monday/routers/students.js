import express from 'express';
const router = express.Router();
let students = [];

const checkForId = (req, res, next) => {
    const id = req.params.id;
    const student = students.find(student => student.id === id);
    if (typeof student === 'undefined') {
        res.status(404).json({ message: 'no one with that id exists in db' });
    } else {
        next();
    }
}

router.get('/', (req, res) => {
    res.status(200).json(students);
});

router.get('/:id', checkForId, (req, res) => {
    const id = req.params.id;
    const student = students.find(student => student.id === id);
    res.status(200).json(student);
});

router.post('/', (req, res) => {
    const student = req.body;
    if (!student.email && student.name) {
        res.status(404).json({ message: 'name and email are required' })
    }
    const newStudent = {
        id: new Date().getTime().toString().substring(4),
        name: student.name,
        email: student.email
    }
    students = [...students, newStudent];
    res.status(201).send(newStudent);
});

router.delete('/:id', checkForId, (req, res) => {
    const id = req.params.id;
    students = students.filter(student => student.id !== id)
    res.status(200).json({ message: 'removed successfully' });
})

router.put('/:id', checkForId, (req, res) => {
    const id = req.params.id;
    const body = req.body;
    let student = students.find(student => student.id === id)
    const newStudent = {
        name: body.name || student.name,
        email: body.email || student.email,
        id: student.id
    }
    students = students.map(student => student.id === id ? newStudent : student);
    res.status(200).json(newStudent);
})

export default router;