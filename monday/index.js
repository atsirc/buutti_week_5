import express from 'express';
const app = express();
const port = 3000;
import studentRouter from './routers/students.js';
let count = 0;
const names = {};

// you never learn, do you?
app.use(express.json())

const errorHandler = (err, _req, res, next) => {
    if (err) {
        console.log(err)
        res.status(404).json(err.message);
    } else {
        next();
    }
}

app.use('/student', errorHandler, studentRouter);

app.use('/counter/:name', (req, res, next) => {
    if (req.params.name === 'christa') {
        res.status('401').send('access denied');
    } else {
        next()
    }
});

app.get('/hello', (_req, res) => {
    res.send('world');
});

app.get('/foo', (_req, res) => {
    res.status('200').json({ answer: 'bar' });
});

app.get('/counter', (req, res) => {
    if (req.query.number) {
        count = Number(req.query.number);
    } else {
        count++;
    }
    res.send(`<h1>Since last start, page has been visited: <span style="color:magenta">${count.toString()}</span> times</h1>`);
});

app.get('/counter/:name', (req, res) => {
    console.log(req.params);
    const name = req.params.name;
    names[name] = names[name] ? names[name] + 1 : 1;
    res.send(`<h1>${name} was here ${names[name]} time${names[name]===1 ? '' : 's'}</h1>`);
});

app.post('/', (req, res) => {
    const body = req.body;
    console.log(body.firstWord)
    res.status(200).send(body);
})


//add unknwon endpoint at last

app.listen(port, () => {
    console.log('hi');
});