import { Router } from 'express';
import { createUser, login } from '../services/userService.js';

const router = Router();

// note to self: have to use async although no database because bcrypt uses async
// using next to handle errors, in books.js the next-calls are in the middleware, here is no middleware som using try catch
router.post('/create', async(req, res, next) => {
  try {
    const result = await createUser(req.body);
    res.status(201).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/login', async(req, res, next) => {
  try {
    const token = await login(req.body);
    res.status(200).json({ token });
  } catch (error) {
    next(error);
  }
});

export default router;