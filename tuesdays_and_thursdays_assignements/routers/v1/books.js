import express from 'express';
const router = express.Router();
import { books } from '../../testdata/books.js';

const checkForId = (req, _res, next) => {
  const id = req.params.id;
  const book = books.find(book => book.id === id);
  req.book = book;
  if (typeof book === 'undefined') {
    // res.status(404).json({ message: 'Book could not be found' });
    throw new Error('requested id cannot be found in the database');
  } else {
    next();
  }
};

const validateBook = (req, _res, next) => {
  const body = req.body;
  if (['name', 'author', 'read'].every(key => body.hasOwnProperty(key))) {
    if (typeof body.read === 'boolean') {
      return next();
    }
  }
  throw new Error('required keys are `name` [string], `author` [string] and `read` [boolean]');
};

router.get('/', (_req, res) => {
  res.status(200).json(books);
});

router.get('/:id', checkForId, (req, res) => {
  const book = req.book;
  return res.status(200).json(book);
});

router.post('/', validateBook, (req, res) => {
  const body = req.body;
  const id = new Date().getTime().toString().substring(5);
  const newBook = {
    id: id,
    name: body.name,
    author: body.author,
    read: body.read
  };
  books.push(newBook);
  return res.status(201).send(newBook);
});

router.put('/:id', checkForId, (req, res) => {
  const id = req.params.id;
  const modifiedBook = req.body;
  if (modifiedBook.read && typeof modifiedBook.read !== 'boolean') {
    throw new Error('wrong values');
  }
  books.forEach(b => {
    if (b.id === id) {
      b.name = modifiedBook.name || b.name;
      b.author = modifiedBook.author || b.author;
      b.read = modifiedBook.read || b.read;
      return res.status(200).json(b);
    }
  });
});

router.delete('/:id', checkForId, (req, res) => {
  for (let i = 0; i < books.length; i++) {
    if (books[i].id === req.params.id) {
      books.splice(i, 1);
      break;
    }
  }
  res.status('200').json(books);
});

export default router;
