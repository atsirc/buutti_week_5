const logger = (req, res, next) => {
  const method = req.method;
  const url = req.url;
  const status = res.statusCode;
  console.log('----------------------------------');
  console.log(`${new Date().toLocaleString()} ${method}:${url} ~ ${status}`);
  next();
};

export default logger;