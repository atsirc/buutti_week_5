import app from '../index.js';
import request from 'supertest';

describe('testing index-endpoints for books api', () => {
  it('get all books returns 200', (done) => {
    request(app).get('/api/v1/books/').expect(200, done);
  });

  it('get specific book with id 1935182722 returns an object that matches test-object', async() => {
    const response = await request(app)
      .get('/api/v1/books/1935182722');
    expect(response.statusCode).toBe(200);
    expect(response.body).toMatchObject({
      'name': 'Android in Action, Second Edition',
      'author': 'W. Frank Ableson',
      'read': false
    });
  });

  it('get specific book with id 193398673 that doesnt exist returns 404', (done) => {
    request(app)
      .get('/api/v1/books/193398673')
      .expect(404, done);
  });

  it('when creating a new book returns 201', (done) => {
    request(app)
      .post('/api/v1/books/')
      .send({ name: 'this name', author: 'some author', read: true })
      .expect(201, done);
  });


  it('when creatng a new book without all keys returns 400', (done) => {
    request(app)
      .post('/api/v1/books/')
      .send({ name: 'this name', author: 'some author'})
      .expect(400, done);
  });

  it('creating a book with string instead of boolean as value for key read returns 400', (done) => {
    request(app)
      .post('/api/v1/books/')
      .send({ name: 'this name', author: 'some author', read: 'true'})
      .expect(400, done);
  });

  it('when updating a book with a none existing id 193518222, return 404', (done) => {
    request(app)
      .put('/api/v1/books/193518222') 
      .send({author: 'some one you don\'t know', name: 'none'})
      .expect(404, done);
  });

  it('when updateing a book with id 1935182722, returns the updated object', (done) => {
    request(app)
      .put('/api/v1/books/1935182722') 
      .send({author: 'some one you don\'t know', name: 'none', read: true})
      .expect(200)
      .expect({
        author: 'some one you don\'t know',
        name: 'none',
        id: '1935182722',
        read: true 
      }, done);
  });

  it('when updateing a book with id 1933988320 only the name should be changed', (done) => {
    request(app)
      .put('/api/v1/books/1933988320') 
      .send({'author': 'Allen, Rob'})
      .expect(200)
      .expect({
        'name': 'Zend Framework in Action',
        'id': '1933988320',
        'author': 'Allen, Rob',
        'read': true
      }, done);
  });

  it('when updateing a book with id 1933988320 with string as type for key read returns 400', (done) => {
    request(app)
      .put('/api/v1/books/1933988320') 
      .send({'read': 'Allen, Rob'})
      .expect(400, done);
  });

  it('when deleting a book returns 200', (done) =>  {
    request(app)
      .delete('/api/v1/books/1935182722')
      .expect(200, done);
  });

  it('when deleting a recently deleted book returns 404', (done) =>  {
    request(app)
      .delete('/api/v1/books/1935182722')
      .expect(404, done);
  });
});

describe('testing user creation', () => {
  it('creating a new user returns object that matches test-case', async () => {
    const response = await request(app)
      .post('/user/create')
      .send({
        'username': 'christa',
        'password': 'test'
      });

    expect(response.statusCode).toBe(201);
    expect(response.body).toHaveProperty('username', 'christa');
    expect(response.body).toHaveProperty('id');
  });

  it('creating a new user without password fails with 400', async () => {
    const response = await request(app)
      .post('/user/create')
      .send({
        'username': 'christa'
      });

    expect(response.statusCode).toBe(400);
  });
  it('test login in with recently creted user returns 200 and a token', async () => {
    const response = await request(app)
      .post('/user/login')
      .send({
        'username': 'christa',
        'password': 'test'
      });

    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('token');
  });
  
  it('test logging in with wrong password returns `something went wrong`', async () => {
    const response = await request(app)
      .post('/user/login')
      .send({
        'username': 'chrsta',
        'password': 'test'
      });

    expect(response.statusCode).toBe(400);
    expect(response.text).toMatch('something went wrong');
  });

  it('test logging in with none existant user returns 400', async () => {
    const response = await request(app)
      .post('/user/login')
      .send({
        'username': 'chrsta',
        'password': 'test'
      });

    expect(response.statusCode).toBe(400);
  });
});

describe('testing endpoint that doesnt exist', () => {
  it('testing endpoint /lksjfd, should return html page with 404 - this url doesn\'t lead anywhere', async () => {
    const response = await request(app)
      .get('/lksjfd');
    expect(response.status).toBe(404);
    expect(response.text).toMatch('<h1>404 - this url doesn\'t lead anywhere</h1>');
  });
});