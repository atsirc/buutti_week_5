import dotenv from 'dotenv';
dotenv.config();
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
// fake databse
const users = [];

export const createUser = async user => {
  const saltRounds = 10;
  if (!user.password || !user.username) {
    throw new Error('required keys are username and password');
  }
  const passHash = await bcrypt.hash(user.password, saltRounds);
  const id = user.username.substring(0, 1) + new Date().getTime().toString();
  const storableUser = {
    id: id,
    username: user.username,
    passHash
  };
  users.push(storableUser);
  return { id: id, username: user.username };
};

export const login = async user => {
  const userFromDb = users.find(u => u.username === user.username);
  const passCorrect = userFromDb ? await bcrypt.compare(user.password, userFromDb.passHash) : false;
  if (!user || !passCorrect) {
    throw new Error('something went wrong');
  }
  const token = jwt.sign({
    id: userFromDb.id,
    username: user.username
  },
  process.env.APP_SECRET
  );
  return token;
};