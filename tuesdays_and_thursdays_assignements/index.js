import path from 'path';
import express from 'express';
import usersRouter from './routers/userRouter.js';
import booksRouter from './routers/v1/books.js';
import requestLogger from './middleware/requestLogger.js';
import helmet from 'helmet';
// using type: module in package.json makes __dirname not work
const __dirname = path.resolve();
// NODE_ENV is defined in package.json
const environment = process.env.NODE_ENV;

const app = express();
const port = 3001;
// app.listen(port, () => console.log('listening to port: ' + port));

const errorLogger = (err, _req, _res, next) => {
  console.log('Error: ', err.message);
  next(err);
};

const errorEndpoint = (err, _req, res, _next) => {
  if (err.message.includes(' id ')) {
    res.status(404).send(`<h1>400 - ${err.message}</h1>`);
  } else {
    res.status(400).send(`<h1>404 - ${err.message}</h1>`);
  }
};

const unknownEndpoint = (_req, res, _next) => {
  res.status(404).sendFile(__dirname + '/html/404.html');
};

app.use('/', express.static(__dirname + '/html'));
app.use(express.json());
app.use(helmet());

environment !== 'test' && app.use(requestLogger);

app.use('/user', usersRouter);
app.use('/api/v1/books', booksRouter);

environment !== 'test' && app.use(errorLogger);

app.use(errorEndpoint);
app.use(unknownEndpoint);

environment !== 'test' && app.listen(port, () =>{
  console.log('Running on port ' + port);
});

export default app;
