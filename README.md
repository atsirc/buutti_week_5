# PART 1 of Thursdays asignement (Part 2 is in branch cicd)
  
* line 58 is tested!  
* Didn't test console logs, because it seemed a bit tricky, and I guess somewhat unnescessary.  
* got jest-runner-eslint to work:
  1) ```npm install jest-runner-eslint --save-dev```
  2) added the following to `jest.config.mjs`
``` javascript
  projects:  [
    {
      displayName: 'test'
    },
    {
      runner: 'jest-runner-eslint',
      displayName: 'lint',
      testMatch: ['./**/*.js']
    }
  ]
```     
  
```
 PASS   test  test/index.test.js
  testing index-endpoints for books api
    √ get all books returns 200 (135 ms)
    √ get specific book with id 1935182722 returns an object that matches test-object (71 ms)
    √ get specific book with id 193398673 that doesnt exist returns 404 (44 ms)
    √ when creating a new book returns 201 (46 ms)
    √ when creatng a new book without all keys returns 400 (31 ms)
    √ creating a book with string instead of boolean as value for key read returns 400 (20 ms)
    √ when updating a book with a none existing id 193518222, return 404 (18 ms)
    √ when updateing a book with id 1935182722, returns the updated object (26 ms)
    √ when updateing a book with id 1933988320 only the name should be changed (25 ms)
    √ when updateing a book with id 1933988320 with string as type for key read returns 400 (23 ms)
    √ when deleting a book returns 200 (30 ms)
    √ when deleting a recently deleted book returns 404 (22 ms)
  testing user creation
    √ creating a new user returns object that matches test-case (93 ms)
    √ creating a new user without password fails with 400 (19 ms)
    √ test login in with recently creted user returns 200 and a token (89 ms)
    √ test logging in with wrong password returns `something went wrong` (35 ms)
    √ test logging in with none existant user returns 400 (62 ms)
  testing endpoint that doesnt exist
    √ testing endpoint /lksjfd, should return html page with 404 - this url doesn't lead anywhere (46 ms)

 PASS   lint  routers/v1/books.js
  √ ESLint (165 ms)

 PASS   lint  coverage/lcov-report/sorter.js
  √ ESLint (75 ms)

 PASS   lint  coverage/lcov-report/prettify.js
  √ ESLint (266 ms)

 PASS   lint  coverage/lcov-report/block-navigation.js
  √ ESLint (24 ms)

 PASS   lint  testdata/books.js
  √ ESLint (8 ms)

 PASS   lint  services/userService.js
  √ ESLint (11 ms)

 PASS   lint  test/index.test.js
  √ ESLint (30 ms)

 PASS   lint  routers/userRouter.js
  √ ESLint (7 ms)

 PASS   lint  middleware/requestLogger.js
  √ ESLint (12 ms)

 PASS   lint  ./index.js
  √ ESLint (16 ms)

----------------------------------|---------|----------|---------|---------|-------------------
File                              | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s 
----------------------------------|---------|----------|---------|---------|-------------------
All files                         |   91.58 |    89.47 |      85 |   91.26 | 
 tuesdays_assignements            |    87.5 |     62.5 |      50 |    87.5 |
  index.js                        |    87.5 |     62.5 |      50 |    87.5 | 17-18,48
 tuesdays_assignements/middleware |   14.28 |      100 |       0 |   14.28 |
  requestLogger.js                |   14.28 |      100 |       0 |   14.28 | 2-7
 tuesdays_assignements/routers    |     100 |      100 |     100 |     100 |
  userRouter.js                   |     100 |      100 |     100 |     100 |
 tuesdays_assignements/routers/v1 |     100 |       95 |     100 |     100 |
  books.js                        |     100 |       95 |     100 |     100 | 58
 tuesdays_assignements/services   |     100 |      100 |     100 |     100 |
  userService.js                  |     100 |      100 |     100 |     100 |
 tuesdays_assignements/testdata   |     100 |      100 |     100 |     100 |
  books.js                        |     100 |      100 |     100 |     100 |
----------------------------------|---------|----------|---------|---------|-------------------
Test Suites: 11 passed, 11 total
Tests:       28 passed, 28 total
Snapshots:   0 total
Time:        4.41 s
Ran all test suites in 2 projects.
```
